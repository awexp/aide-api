import GenericRegistry from './GenericRegistry'

export default class Project {
    constructor() {
        this.pageRegistry = new GenericRegistry()
        this.pageElementsRegistry = new GenericRegistry()
        this.pageExtensionsRegistry = new GenericRegistry()
    }

    getPageRegistry() {
        return this.pageRegistry
    }

    getPageExtensionsRegistry() {
        return this.pageExtensionsRegistry
    }

    getPageElementsRegistry() {
        return this.pageElementsRegistry
    }
}