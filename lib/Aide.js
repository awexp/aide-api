import Project from './Project'

export default {
    Project: Project,
    Page: window.AidePage,
    PageExtension: window.AidePageExtension,
    PageElement: window.AidePageElement
}