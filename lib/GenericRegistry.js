export default class GenericRegistry {
    constructor() {
        this.items = {}
    }

    /**
     * Adds an entry to the registry.
     *
     * @param {string} name
     * @param {Object} prototype
     */
    register(name, prototype) {
        this.items[name] = prototype
    }

    /**
     *
     * @param {string} name
     * @return {Object}
     */
    find(name) {
        return this.items[name]
    }
}